<?php

/**
 * @file
 * Node from workflow CTools relationship.
 */

/**
 * Implementation of hook_ctools_relationships().
 */
function workflow_relationships_node_from_workflow_ctools_relationships() {
  $desc = t('This relationship provides another node context based on workflow information from existing node context.');
  return array(
    'title' => t('Node from workflow'),
    'keyword' => 'workflow',
    'description' => $desc,
    'required context' => new ctools_context_required(t('Node'), 'node'),
    'context' => 'workflow_relationships_node_from_workflow_context',
    'settings form' => 'workflow_relationships_node_from_workflow_settings_form',
    'settings form validate' => 'workflow_relationships_node_from_workflow_settings_form_validate',
  );
}

/**
 * Return a new ctools context based on an existing context.
 */
function workflow_relationships_node_from_workflow_context($context = NULL, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL
  if (empty($context->data)) {
    return ctools_context_create_empty('node', NULL);
  }
  if ($nid = workflow_relationships_relationship($context->data)) {
    if ($node = node_load($nid)) {
      return ctools_context_create('node', $node);
    }
  }
}

/**
 * Settings form for the ctools relationship.
 */
function workflow_relationships_node_from_workflow_settings_form($conf) {
  $placeholders = array('!page' => l(t('Workflow Relationships administration page'), 'admin/build/workflow_relationships'));
  $desc .= '<br/>'. t('<strong>Hint:</strong> you can setup this relationship at the !page.', $placeholders) .'<br/><br/>';
  $form['dummy'] = array(
    '#title' => t('Relationship settings'),
    '#value' => $desc,
  );
  return $form;
}
