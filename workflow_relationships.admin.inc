<?php

/**
 * @file
 * Administration page code for Workflow Relationships.
 */

/**
 *  Workflow Relationships form
 */
function workflow_relationships_admin_form() {
  $form = array();
  $form['node_relationships'] = array(
    '#type' => 'fieldset',
    '#title' => t('Workflow to node relationships'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -10,
  );
  $description  = t('Setup multiple workflow to node relationships. Parameters can be specific or of "any" type.') .'<br/>';
  $description .= t('When searching for proper relationship for a node workflow, relationships table will be traversed from top to bottom, until 1st match if found.');
  $description .= '<br/>'. t('"Any" type values will always count as match, so be careful to put them below specific ones.');
  $form['node_relationships']['#description'] = $description;
  $form['node_relationships']['#description'] .= '<br/><br/>';
  $form['node_relationships']['workflow_relationships_nodes'] = array(
    '#type' => 'hierarchical_select',
    '#title' => t('Select type and workflow of source node'),
    '#size' => 1,
    '#description' => t('Select node type, workflow, previous and current state'),
    '#config' => array(
      'module' => 'workflow_relationships',
      'params' => array(
      ),
      'save_lineage'    => 1,
      'enforce_deepest' => 1,
      'entity_count'    => 0,
      'resizable'       => 1,
      'level_labels' => array(
        'status' => 0,
        'labels' => array(
          0 => t('Node type'),
          1 => t('Workflow'),
          2 => t('Old state'),
          3 => t('Current state'),
        ),
      ),
      'dropbox' => array(
        'status'   => 0,
        'title'    => '',
        'limit'    => 0,
        'reset_hs' => 1,
      ),
      'editability' => array(
        'status'           => 0,
        'item_types'       => array(),
        'allowed_levels'   => array(
          0 => 0,
          1 => 0,
          2 => 0,
          3 => 0
        ),
        'allow_new_levels' => 0,
        'max_levels'       => 4,
      ),
      // These settings cannot be configured through the UI: they can only be
      // overridden through code.
      'animation_delay'    => 400,
      'exclusive_lineages' => array(),
      'render_flat_select' => 0,
    ),
  );
  $form['node_relationships']['target_node'] = array(
    '#type' => 'textfield',
    '#title' => t('Target node id'),
    '#size' => 20,
    '#maxlength' => 20,
  );
  $form['node_relationships']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add relationship'),
    '#submit' => array('workflow_relationships_node_add_submit'),
    '#validate' => array('workflow_relationships_node_add_validate'),
    '#suffix' => '<br/><br/>',
  );
  
  // Relationships table form element
  $form['node_relationships']['list'] = workflow_relationships_node_table_form();

  return $form;
}

/**
 *  Form builder for relationships table
 */
function workflow_relationships_node_table_form() {
  $form = array();
  
  // Get workflow information from DB
  $workflows = workflow_get_all();
  foreach($workflows as $wid => $wname) {
    $states[$wid] = workflow_get_states($wid);
  }

  // Get relationship table
  $relationships = variable_get('workflow_relationships', array());

  // Node types defined in DB
  $types = node_get_types('names');
  
  $form['rows'] = array();

  // Build relationship table rows
  foreach ($relationships as $delta => $values) {
    $weight = $values['weight'];
    $type   = $values['type'];
    $wid    = $values['workflow'];
    $prev   = $values['prev_state'];
    $cur    = $values['cur_state'];
    $nid    = $values['nid'];

    $form['rows'][$delta] = array();

    // Node type column
    $form['rows'][$delta]['type'] = array();
    if (!empty($types[$type])) {
      $form['rows'][$delta]['type']['#value'] = $types[$type];
    }
    else {
      if ($type == '*') {
        $form['rows'][$delta]['type']['#value'] = t('any type');
      }
      else {
        $form['rows'][$delta]['type']['#value'] = '<span class="error">'. t('Invalid node type') ." ($type)</span>";
        drupal_set_message(t('You have one or more invalid entries in workflow-node relationships table.'), 'error', FALSE);
      }
    }

    // Workflow column
    $form['rows'][$delta]['workflow'] = array();
    if (!empty($workflows[$wid])) {
      $form['rows'][$delta]['workflow']['#value'] = $workflows[$wid];
    }
    else {
      if ($wid == '*') {
        $form['rows'][$delta]['workflow']['#value'] = t('any workflow');
      }
      else {
        $form['rows'][$delta]['workflow']['#value'] = '<span class="error">'. t('Workflow not found') ." (wid $wid)</span>";
        drupal_set_message(t('You have one or more invalid entries in workflow-node relationships table.'), 'error', FALSE);
      }
    }

    // Previous state column
    $form['rows'][$delta]['prev_state'] = array();
    if (isset($states[$wid][$prev])) {
      $form['rows'][$delta]['prev_state']['#value'] = $states[$wid][$prev];
    }
    else {
      if ($prev == '*') {
        $form['rows'][$delta]['prev_state']['#value'] = t('any state');
      }
      else {
        $form['rows'][$delta]['prev_state']['#value'] = '<span class="error">'. t('State not found') ." (sid $prev)</span>";
        drupal_set_message(t('You have one or more invalid entries in workflow-node relationships table.'), 'error', FALSE);
      }
    }

    // Current state column
    $form['rows'][$delta]['cur_state'] = array();
    if (isset($states[$wid][$cur])) {
      $form['rows'][$delta]['cur_state']['#value'] = $states[$wid][$cur];
    }
    else {
      if ($cur == '*') {
        $form['rows'][$delta]['cur_state']['#value'] = t('any state');
      }
      else {
        $form['rows'][$delta]['cur_state']['#value'] = '<span class="error">'. t('State not found') ." (sid $cur)</span>";
        drupal_set_message(t('You have one or more invalid entries in workflow-node relationships table.'), 'error', FALSE);
      }
    }

    // Target node
    $form['rows'][$delta]['node'] = array();
    if ($node = node_load($nid)) {
      $form['rows'][$delta]['node']['#value'] = l($node->title, 'node/'.$nid) ." (nid $nid)";
    }
    else {
      $form['rows'][$delta]['node']['#value'] = '<span class="error">'. t('Node not found') ." (nid $nid)</span>";
      drupal_set_message(t('You have one or more invalid entries in workflow-node relationships table.'), 'error', FALSE);
    }

    // Delete checkbox
    $form['rows'][$delta]['select'.$delta] = array(
      '#type' => 'checkbox',
      '#default_value' => 0,
    );

    // Weight column
    $form['rows'][$delta]['weight' .$delta] = array(
      '#type' => 'weight',
      '#delta' => 50,
      '#default_value' => $weight,
    );
  }

  $form['#theme'] = 'workflow_relationships_table';
  $form['buttons'] = array();
  $form['buttons']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('workflow_relationships_table_save_submit'),
    '#validate' => array('workflow_relationships_table_save_validate'),
    '#prefix' => '<br/>',
  );

  return $form;
}

/**
 *  Validate remove relationships form values
 */
function workflow_relationships_table_save_validate($form, &$form_state) {
  // TODO: Is this validation needed ?
}

/**
 *  Validate add wf-node relationship form values
 */
function workflow_relationships_node_add_validate($form, &$form_state) {
  if (!isset($form_state['values']['workflow_relationships_nodes'][0]) || $form_state['values']['workflow_relationships_nodes'][0] == 'none') {
    form_set_error('workflow_relationships_nodes', t('You must select a node type for relationship.'));
  }
  if (!isset($form_state['values']['workflow_relationships_nodes'][1]) || $form_state['values']['workflow_relationships_nodes'][1] == 'wf_0') {
    form_set_error('workflow_relationships_nodes', t('You must select a workflow for relationship.'));
  }
  if (!isset($form_state['values']['workflow_relationships_nodes'][2]) || $form_state['values']['workflow_relationships_nodes'][2] == '') {
    form_set_error('workflow_relationships_nodes', t('You must select previous state for relationship.'));
  }
  if (!isset($form_state['values']['workflow_relationships_nodes'][3]) || $form_state['values']['workflow_relationships_nodes'][3] == '') {
    form_set_error('workflow_relationships_nodes', t('You must select current state for relationship.'));
  }
  if (!$node = node_load($form_state['values']['target_node'])) {
    form_set_error('target_node', t('You must select valid node id.'));
  }
}

/**
 *  Submit callback for removing workflow-node relationships from the table
 */
function workflow_relationships_table_save_submit($form, &$form_state) {
  $relationships = variable_get('workflow_relationships', array());
  $relationships_new = array();
  foreach ($relationships as $delta => $values) {
    if (isset($form_state['values']['select'.$delta])) {
      if ($form_state['values']['select'.$delta] == 0) {
        $values['weight'] = $form_state['values']['weight'.$delta];
        $relationships_new[] = $values;
      }
    } 
    else {
      // Missing entry found: something is wrong.
      // TODO: Insert some error reporting here.
    }
  }
  usort($relationships_new, '_sort_wf_relationships');
  variable_set('workflow_relationships', $relationships_new);
  drupal_set_message(t('The workflow-node relationships table was updated.'));
  $form_state['redirect'] = 'admin/build/workflow_relationships';
}

/**
 * Submit callback for adding new workflow-node relationship
 */
function workflow_relationships_node_add_submit($form, &$form_state) {
  $table = variable_get('workflow_relationships', array());

  if (preg_match('/^node_(.*)/', $form_state['values']['workflow_relationships_nodes'][0], $matches)) {
    $type = $matches[1];
  }
  if (preg_match('/^wf_(.*)/', $form_state['values']['workflow_relationships_nodes'][1], $matches)) {
    $workflow = $matches[1];
  }
  if (preg_match('/^prev_state_([\d\*]+)_(.*)/', $form_state['values']['workflow_relationships_nodes'][2], $matches)) {
    $prev_state = $matches[2];
  }
  if (preg_match('/^cur_state_([\d\*]+)_(.*)/', $form_state['values']['workflow_relationships_nodes'][3], $matches)) {
    $cur_state = $matches[2];
  }

  $table[] = array(
    'weight' => 0,
    'type' => $type,
    'workflow' => $workflow,
    'prev_state' => $prev_state,
    'cur_state' => $cur_state,
    'nid' => $form_state['values']['target_node'],
  );
  usort($table, '_sort_wf_relationships');
  variable_set('workflow_relationships', $table);
  drupal_set_message(t('The workflow-node relationship was added.'));
  $form_state['redirect'] = 'admin/build/workflow_relationships';
}

/******************* Theme functions ***********************/

/**
 *  Theme relationships table
 */
function theme_workflow_relationships_table($form) {
  $rows = array();
  foreach (element_children($form['rows']) as $delta) {
    $values = $form['rows'][$delta];

    // Weight column
    $values['weight'.$delta]['#attributes']['class'] = 'relationship-weight';
    $rows[$delta] = array();
    $rows[$delta]['data'] = array();
    $rows[$delta]['class'] = 'draggable';
    foreach (element_children($values) as $col) {
      $rows[$delta]['data'][$col] = drupal_render($values[$col]);
    }
  }
  unset($form['rows']);
  $header = array(t('Content Type'), t('Workflow'), t('Previous state'), t('Current state'), t('Node'), t('Remove'), t('Weight'));
  $attributes = array(
    'id' => 'worflow-relationships-table',
  );
  $output = theme('table', $header, $rows, $attributes);
  drupal_add_tabledrag('worflow-relationships-table', 'order', 'sibling', 'relationship-weight', NULL, NULL, TRUE);
  return $output . drupal_render($form);
}


/******************* Hierarchical Select implementation ****/

/**
 *   Returns an array with the names of all parameters that are necessary for
 *   this implementation to work.
  */
function workflow_relationships_hierarchical_select_params() {
  return;
}

/**
 * Returns the root level of the hierarchy: an array of (item, label) pairs.
 * The $dropbox parameter is optional and can even ommitted, as it's only
 * necessary if you need the dropbox to influence your hierarchy
 *
 * @param $params
 * @param $dropbox
 * @return unknown_type
 */
function workflow_relationships_hierarchical_select_root_level($params, $dropbox = FALSE) {
  $type_map = array();
  $result = db_query("SELECT wid, type FROM {workflow_type_map}");
  while ($data = db_fetch_object($result)) {
    $type_map[$data->type] = $data->wid;
  }
  $root_items = array();
  foreach (node_get_types('names') as $type => $name) {
    if (isset($type_map[$type]) && !empty($type_map[$type])) {
      $root_items['node_' . $type] = $name;
    }
  }
  $root_items['node_*'] = t('any node type');
  return $root_items;
}

/**
 * Gets the children of $parent ($parent is an item in the hierarchy) and
 * returns them: an array of (item, label) pairs, or the empty array if the
 * given $parent has no children.
 * The $dropbox parameter is optional and can even ommitted, as it's only
 * necessary if you need the dropbox to influence your hierarchy.
 *
 * @param $parent
 * @param $params
 * @param $dropbox
 * @return unknown_type
 */
function workflow_relationships_hierarchical_select_children($parent, $params, $dropbox = FALSE) {
  $children = array();

  if (preg_match('/^node_(.*)/', $parent, $matches)) {
    // Workflow level
    $workflows = workflow_get_all();
    $type_map = array();
    if ($match[1] == '*') {
      $children = $workflows;
    } 
    else {
      $result = db_query("SELECT wid FROM {workflow_type_map} WHERE type = %d", $matches[1]);
      while ($data = db_fetch_object($result)) {
        if ($data->wid != 0) {
          $children['wf_'.$data->wid] = $workflows[$data->wid];
        }
      }
    }
    $children['wf_*'] = t('any workflow');
    return $children;
  }
  
  if (preg_match('/^wf_(.*)/', $parent, $matches)) {
    // Previous state level
    if ($matches[1] != '*') {
      $states = workflow_get_states($matches[1]);
      $states['*'] = 'any state';
      foreach($states as $id => $name) {
        $item = 'prev_state_'. $matches[1] .'_'. $id;
        $children[$item] = $name;
      }
    }
    if ($matches[1] == '*') {
      $children['prev_state_*_*'] = t('any state');
    }
    return $children;
  }
  
  if (preg_match('/^prev_state_([\d\*]+)_(.*)/', $parent, $matches)) {
    // Current state level
    if ($matches[1] != '*') {
      $states = workflow_get_states($matches[1]);
      $states['*'] = t('any state');
      foreach($states as $id => $name) {
        $item = 'cur_state_'. $matches[1] .'_'. $id;
        $children[$item] = $name;
      }
    }
    if ($matches[1] == '*') {
      $children['cur_state_*_*'] = t('any state');
    }
    return $children;
  }

  // No match, return empty array;
  return $children;
}

/**
 * Validates an item, returns TRUE if valid, FALSE if invalid.
 *
 * @param $item
 * @param $params
 * @return unknown_type
 */
function workflow_relationships_hierarchical_select_valid_item($item, $params) {
  if (!empty($item)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Return metadata about this implementation.
 * This information is used to generate the implementations overview at
 * admin/settings/hierarchical_select/implementations.
 *
 * @return unknown_type
 */
function workflow_relationships_hierarchical_select_implementation_info() {
  return array(
    'hierarchy type' => t('Workflow'),
    'entity type'    => t('Workflow states'),
    'entity'         => '',
    'context type'   => t('Workflow Panes configuration'),
    'context'        => t(''),
  );
}

/************** Private functions ***********************/

/**
 *  Sort relationships
 */
function _sort_wf_relationships($a,$b) {
  if ($a['weight'] == $b['weight']) {
       return 0;
  }
  return ($a['weight'] < $b['weight']) ? -1 : 1;
}
